
# coding: utf-8

# # Žvalgomoji analizė

# In[1]:

import pandas as pd 


# In[2]:

import numpy as np 


# In[3]:

import IPython 
import math 
from ggplot import * 
get_ipython().magic('matplotlib inline') 


# In[4]:

df = pd.read_csv("finaldata.csv") 


# In[5]:

list(df.columns.values) 


# In[6]:

del df['Unnamed: 0'] 


# In[7]:

df.describe().transpose() 


# In[8]:

df1 = df[['user_lifetime']] 


# In[9]:

df1 


# In[10]:

lifetime=df1.apply(lambda x: x.value_counts(dropna=False)) 


# In[11]:

lifetime 


# In[12]:

ggplot(aes(x='user_lifetime'), data=df1) + geom_bar(binwidth=1000) 


# In[13]:

churned=df[df.churn == 1] 


# In[14]:

active=df[df.churn == 0] 


# In[15]:

active.describe().transpose() 


# In[16]:

churned.describe().transpose() 


# In[17]:

df2=churned[['user_no_outgoing_activity_in_days']] 


# In[18]:

df2.columns 


# In[19]:

ggplot(aes(x='user_no_outgoing_activity_in_days'), data=df2) + geom_bar(binwidth=20)


# In[20]:

df3=active[['user_no_outgoing_activity_in_days']] 


# In[21]:

df3.columns 


# In[22]:

ggplot(aes(x='user_no_outgoing_activity_in_days'), data=df3) + geom_bar(binwidth=20)


# In[ ]:



