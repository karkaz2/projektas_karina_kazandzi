
# coding: utf-8

# # Projektas

# # Karina Kazandži 

# Direktorijos struktūra

# In[1]:

get_ipython().system(' tree -La 2 ..')


# In[2]:

import os
import IPython
import math
import pandas as pd
import ggplot as gg


# In[3]:

get_ipython().system(' ls')


# In[4]:

dataDF = pd.read_csv("data/customer_usage_005.csv")


# In[5]:

dataDF.describe().transpose()


# In[6]:

usageDF = (
    sqlContext.read.format("com.databricks.spark.csv")
    .options(inferSchema=True, header=True)
    .load("data/customer_usage_005.csv")
)


# In[7]:

usageDF


# Sukuriame laikiną lentelę.

# In[8]:

usageDF.registerTempTable("customer_usage")


# Datos kintamieji.

# In[9]:

dateCols = ["year", "month"]


# Vartotojo ID kintamasis, saugome `list`'e , kad būtų paprasčiau apjunti vėliau.

# In[10]:

idCols = ["user_account_id"]


# Binariniai kintamieji.

# In[11]:

binaryCols = [
    "user_intake",
    "user_has_outgoing_calls", "user_has_outgoing_sms", 
    "user_use_gprs", "user_does_reload"
]


# 
# Sukuriame tolydžiūjų kintamųjų `list`'ą.

# In[12]:

categoricalCols = dateCols + binaryCols + idCols

continuousCols = [c for c in usageDF.columns if c not in categoricalCols]


# Sukuriame vidurkių SQL išraikškas tolydiesiems kintamiesiems.

# In[13]:

sqlExprsMeanCols = ["AVG({0}) AS {0}".format(c) for c in continuousCols]
sqlExprsMeanCols


# Sukuriame maksimumo SQL išraiškas binariniams kintamiesiems.

# In[14]:

sqlExprsBinaryCols = ["MAX({0}) AS {0}".format(c) for c in binaryCols]
sqlExprsBinaryCols


# Sukuriame mėnesių [vartotojo išrašų] skaičiavimo SQL išraišką.

# In[15]:

sqlExprsDateCols = ["COUNT(*) AS n_months"]
sqlExprsDateCols


# Apjungiame visas SQL išraiškas.

# In[16]:

sqlSelectExprs = sqlExprsMeanCols + sqlExprsBinaryCols + sqlExprsDateCols
sqlSelectExprs


# In[17]:

sqlSelect = ", ".join(sqlSelectExprs)
sqlSelect


# SQL SELECT išraiškas įstatome į SQL užklausą ir ją įvykdome.

# In[18]:

aggregatedUsageDF = sqlContext.sql("""
    SELECT user_account_id, {}
    FROM customer_usage
    GROUP BY user_account_id
""".format(sqlSelect))


# In[19]:

aggregatedUsageDF.first()


# 
# Užtikriname, kad direktorija, kurioje saugosime duomenis neegzisuotja.

# In[20]:

get_ipython().system(' rm -rf output/aggregated_customer_usage/')


# Išsaugome agreguotus pagal vartojoją duomenis.

# aggregatedUsageForWritingDF.write.format("com.databricks.spark.csv").save("../data/aggregated_customer_usage")

# In[21]:



aggregatedUsageDF.write.format("com.databricks.spark.csv").save("output/aggregated_customer_usage")


# In[22]:

get_ipython().system(' head -n 1 output/aggregated_customer_usage/part-00000')


# In[23]:

get_ipython().system(' ls output/aggregated_customer_usage/')


# Kadangi duomenys išsagojami į direktoriją po failą kiekvienai Spark duomenų particijai, atskirai išsaugojame stulpelių pavadinimus.

# In[24]:

with open("output/header__aggregated_customer_usage.txt", "w") as f:
    f.write(",".join(aggregatedUsageDF.columns))


# In[25]:

aggregatedUsageNoColumnsDF = (
    sqlContext.read.format("com.databricks.spark.csv")
    .options(inferSchema=True, header=False)
    .load("output/aggregated_customer_usage/")
)


# In[26]:

aggregatedUsageNoColumnsDF.rdd.first()


# Nuskaitome stulpelių pavadinimus ir sukuriame jų `list`'ą

# In[27]:

with open("output/header__aggregated_customer_usage.txt") as f:
    columns = f.read().split(",")


# In[28]:

columns


# Nuskaitome duomenis, konvertuojame `pyspark.sql.DataFrame` į RDD, ir vėl paverčiame į `pyspark.sql.DataFrame` nurodydami stulpelių pavadinimus.

# In[29]:

aggregatedUsageDF2 = (
    sqlContext.read.format("com.databricks.spark.csv")
    .options(inferSchema=True, header=False)
    .load("output/aggregated_customer_usage/")
    .rdd
    .toDF(columns)  
)


# In[30]:

with open("output/columns_continuous__agg_usage.txt", "w") as f:
    f.write(",".join(continuousCols))
with open("output/columns_binary__agg_usage.txt", "w") as f:
    f.write(",".join(continuousCols))
with open("output/columns_ids__agg_usage.txt", "w") as f:
    f.write(",".join(idCols))
with open("output/columns_misc__agg_usage.txt", "w") as f:
    f.write("n_months")


# In[31]:

aggregatedUsageDF2.first()


# Patogumo dėlei išsauogojame kintamųjų pavadinimus pagal jų tipus.

# In[32]:

import os

import numpy as np

from pyspark.ml.feature import VectorAssembler, StandardScaler
from pyspark.mllib.linalg import DenseVector, Vectors


# In[33]:

with open("output/header__aggregated_customer_usage.txt") as f:
    columnsAggUsageDF = f.read().split(",")

aggUsageDF = (
    sqlContext.read.format("com.databricks.spark.csv")
    .options(inferSchema=True)
    .load("output/aggregated_customer_usage/")
    .rdd
    .toDF(columnsAggUsageDF)
)


# In[34]:

aggUsageDF


# In[35]:

aggUsageDF.first()


# In[36]:

def ensureDenseFeatures(featurizedDF, featuresCol="features"):
    columns = featurizedDF.columns
    idxCol = columns.index(featuresCol)
    transformedRow = pyspark.sql.Row(*columns)
    
    def rowColumnSparseToDense(row):
        values = list(row)
        if isinstance(values[idxCol], DenseVector):
            return row
        values[idxCol] = pyspark.mllib.linalg.DenseVector(row[idxCol].toArray())
        return transformedRow(*values)
    
    featurizedDF = featurizedDF.rdd.map(rowColumnSparseToDense).toDF()
    
    return featurizedDF


# In[37]:

def rowColSparseToDense(row, colname="features"):
    columns = row.__fields__
    transformedRow = pyspark.sql.Row(*columns)
    idxCol = columns.index(colname)
    values = list(row)
    values[idxCol] = pyspark.mllib.linalg.DenseVector(r[idxCol].toArray())
    return transformedRow(*values)


# In[38]:

with open("output/columns_continuous__agg_usage.txt") as f:
    continouosColumns = f.read().split(",")
    
with open("output/columns_binary__agg_usage.txt") as f:
    binaryColumns = f.read().split(",")
    
with open("output/columns_ids__agg_usage.txt") as f:
    idsColumns = f.read().split(",")

with open("output/columns_misc__agg_usage.txt") as f:
    miscColumns = f.read().split(",")


# In[39]:

assembler = VectorAssembler(inputCols=continouosColumns, outputCol="features")


# In[40]:

featurizedDF = assembler.transform(aggUsageDF)


# In[41]:

featurizedDF.first()


# In[42]:

scaler = StandardScaler(withMean=True, withStd=True, inputCol="features", outputCol="scaled_features")


# In[43]:

scalerModelOrg = scaler.fit(featurizedDF)


# In[44]:

scalerModelOrg.mean


# In[45]:

scalerModelOrg.std


# In[46]:

featuresEnsuredDF = ensureDenseFeatures(featurizedDF)


# In[47]:

scalerModelEnsured = scaler.fit(featurizedDF)


# In[48]:

scalerModelEnsured.mean


# In[49]:

scalerModelEnsured.std


# In[50]:

featurizedScaledDF = scalerModelOrg.transform(featurizedDF)


# In[51]:

featurizedScaledEnsuredDF = scalerModelEnsured.transform(featuresEnsuredDF)


# In[52]:

featurizedScaledDF.first()


# In[53]:

scalerModel = scalerModelEnsured
featurizedScaledDF = featurizedScaledEnsuredDF

Pradinių požymių reikšmių atstatymas po standartizavimo
# In[54]:

r = featurizedScaledDF.first()


# In[55]:

r.scaled_features


# In[56]:

rescaled_features = (r.scaled_features * scalerModel.std + scalerModel.mean)
rescaled_features


# In[57]:

sum(r.features - rescaled_features)


# In[58]:

scalerMean = " ".join(map(str, scalerModel.mean))
scalerMean


# In[59]:

scalerStd = " ".join(map(str, scalerModel.std))
scalerStd


# In[60]:

os.makedirs(os.path.join(os.getcwd(), "models"), exist_ok=True)


# In[61]:

with open(os.path.join("models", "clustering_scaler__mean.txt"), "w") as f:
    f.write(scalerMean)
    
with open(os.path.join("models", "clustering_scaler__std.txt"), "w") as f:
    f.write(scalerStd)


# In[62]:

with open("models/clustering_scaler__mean.txt") as f:
    meanFeatures = Vectors.dense(*map(float, f.read().split()))
    
with open("models/clustering_scaler__std.txt") as f:
    stdFeatures = Vectors.dense(*map(float, f.read().split()))


# In[63]:

np.allclose(meanFeatures, scalerModel.mean)


# In[64]:

np.allclose(stdFeatures, scalerModel.std)


# In[65]:

scaledRow = pyspark.sql.Row(*(["user_account_id"] + continouosColumns))


# In[66]:

aggUsageScaledDF = (
    featurizedScaledDF.select("user_account_id", "scaled_features")
    .rdd
    .map(lambda r: scaledRow(r.user_account_id, *map(float, r.scaled_features.toArray())))
    .toDF()
).cache()


# In[67]:

aggUsageScaledDF.count()


# In[68]:

with open(os.path.join("output", "header__aggregated_scaled_customer_usage.txt"), "w") as f:
    f.write(",".join(aggUsageScaledDF.columns))


# In[69]:

get_ipython().system(' rm -rf output/aggregated_scaled_customer_usage/')


# In[70]:

aggUsageScaledDF.write.format("com.databricks.spark.csv").save("output/aggregated_scaled_customer_usage")


# In[71]:

with open(os.path.join("output", "header__aggregated_scaled_customer_usage.txt")) as f:
    columnsAggUsageScaledFromDiskDF = f.read().split(",")

aggUsageScaledFromDiskDF = (
    sqlContext.read.format("com.databricks.spark.csv")
    .options(inferSchema=True)
    .load("output/aggregated_scaled_customer_usage/")
    .rdd
    .toDF(columnsAggUsageScaledFromDiskDF)
)


# In[72]:

aggUsageScaledFromDiskDF.first()


# In[73]:

aggUsageScaledFeaturizedFromDiskDF = assembler.transform(aggUsageScaledFromDiskDF)


# In[74]:

meanFeatures


# In[75]:

stdFeatures


# # Kmeans

# In[76]:

from pyspark.ml.feature import StandardScaler, VectorAssembler


# In[77]:

aggUsageScaledDF.rdd.take(1)


# In[78]:

assembler = VectorAssembler(inputCols=["user_lifetime", "user_no_outgoing_activity_in_days", "user_account_balance_last", "user_spendings", "reloads_inactive_days", "reloads_count", "reloads_sum", "calls_outgoing_count", "calls_outgoing_spendings", "calls_outgoing_duration", "calls_outgoing_spendings_max", "calls_outgoing_duration_max", "calls_outgoing_inactive_days", "calls_outgoing_to_onnet_count", "calls_outgoing_to_onnet_spendings", "calls_outgoing_to_onnet_duration", "calls_outgoing_to_onnet_inactive_days", "calls_outgoing_to_offnet_count", "calls_outgoing_to_offnet_spendings", "calls_outgoing_to_offnet_duration", "calls_outgoing_to_offnet_inactive_days", "calls_outgoing_to_abroad_count", "calls_outgoing_to_abroad_spendings", "calls_outgoing_to_abroad_duration", "calls_outgoing_to_abroad_inactive_days", "sms_outgoing_count", "sms_outgoing_spendings", "sms_outgoing_spendings_max", "sms_outgoing_inactive_days", "sms_outgoing_to_onnet_count", "sms_outgoing_to_onnet_spendings", "sms_outgoing_to_onnet_inactive_days", "sms_outgoing_to_offnet_count", "sms_outgoing_to_offnet_spendings", "sms_outgoing_to_offnet_inactive_days", "sms_outgoing_to_abroad_count", "sms_outgoing_to_abroad_spendings", "sms_outgoing_to_abroad_inactive_days", "sms_incoming_count", "sms_incoming_spendings", "sms_incoming_from_abroad_count", "sms_incoming_from_abroad_spendings", "gprs_session_count", "gprs_usage", "gprs_spendings", "gprs_inactive_days", "last_100_reloads_count", "last_100_reloads_sum", "last_100_calls_outgoing_duration", "last_100_calls_outgoing_to_onnet_duration", "last_100_calls_outgoing_to_offnet_duration", "last_100_calls_outgoing_to_abroad_duration", "last_100_sms_outgoing_count", "last_100_sms_outgoing_to_onnet_count", "last_100_sms_outgoing_to_offnet_count", "last_100_sms_outgoing_to_abroad_count", "last_100_gprs_usage"], outputCol="features")


# In[79]:

FeaturizedDF = assembler.transform(aggUsageScaledDF)


# In[80]:

FeaturizedDF.first()


# In[81]:

FeaturizedDF.show(1)


# In[82]:

sf = FeaturizedDF.first().features

sf


# In[83]:

list(map(float, sf))


# In[84]:

pyspark.sql.Row("user_lifetime", "user_no_outgoing_activity_in_days", "user_account_balance_last", "user_spendings", "reloads_inactive_days", "reloads_count", "reloads_sum", "calls_outgoing_count", "calls_outgoing_spendings", "calls_outgoing_duration", "calls_outgoing_spendings_max", "calls_outgoing_duration_max", "calls_outgoing_inactive_days", "calls_outgoing_to_onnet_count", "calls_outgoing_to_onnet_spendings", "calls_outgoing_to_onnet_duration", "calls_outgoing_to_onnet_inactive_days", "calls_outgoing_to_offnet_count", "calls_outgoing_to_offnet_spendings", "calls_outgoing_to_offnet_duration", "calls_outgoing_to_offnet_inactive_days", "calls_outgoing_to_abroad_count", "calls_outgoing_to_abroad_spendings", "calls_outgoing_to_abroad_duration", "calls_outgoing_to_abroad_inactive_days", "sms_outgoing_count", "sms_outgoing_spendings", "sms_outgoing_spendings_max", "sms_outgoing_inactive_days", "sms_outgoing_to_onnet_count", "sms_outgoing_to_onnet_spendings", "sms_outgoing_to_onnet_inactive_days", "sms_outgoing_to_offnet_count", "sms_outgoing_to_offnet_spendings", "sms_outgoing_to_offnet_inactive_days", "sms_outgoing_to_abroad_count", "sms_outgoing_to_abroad_spendings", "sms_outgoing_to_abroad_inactive_days", "sms_incoming_count", "sms_incoming_spendings", "sms_incoming_from_abroad_count", "sms_incoming_from_abroad_spendings", "gprs_session_count", "gprs_usage", "gprs_spendings", "gprs_inactive_days", "last_100_reloads_count", "last_100_reloads_sum", "last_100_calls_outgoing_duration", "last_100_calls_outgoing_to_onnet_duration", "last_100_calls_outgoing_to_offnet_duration", "last_100_calls_outgoing_to_abroad_duration", "last_100_sms_outgoing_count", "last_100_sms_outgoing_to_onnet_count", "last_100_sms_outgoing_to_offnet_count", "last_100_sms_outgoing_to_abroad_count", "last_100_gprs_usage")


# In[85]:

pyspark.sql.Row("user_lifetime", "user_no_outgoing_activity_in_days", "user_account_balance_last", "user_spendings", "reloads_inactive_days", "reloads_count", "reloads_sum", "calls_outgoing_count", "calls_outgoing_spendings", "calls_outgoing_duration", "calls_outgoing_spendings_max", "calls_outgoing_duration_max", "calls_outgoing_inactive_days", "calls_outgoing_to_onnet_count", "calls_outgoing_to_onnet_spendings", "calls_outgoing_to_onnet_duration", "calls_outgoing_to_onnet_inactive_days", "calls_outgoing_to_offnet_count", "calls_outgoing_to_offnet_spendings", "calls_outgoing_to_offnet_duration", "calls_outgoing_to_offnet_inactive_days", "calls_outgoing_to_abroad_count", "calls_outgoing_to_abroad_spendings", "calls_outgoing_to_abroad_duration", "calls_outgoing_to_abroad_inactive_days", "sms_outgoing_count", "sms_outgoing_spendings", "sms_outgoing_spendings_max", "sms_outgoing_inactive_days", "sms_outgoing_to_onnet_count", "sms_outgoing_to_onnet_spendings", "sms_outgoing_to_onnet_inactive_days", "sms_outgoing_to_offnet_count", "sms_outgoing_to_offnet_spendings", "sms_outgoing_to_offnet_inactive_days", "sms_outgoing_to_abroad_count", "sms_outgoing_to_abroad_spendings", "sms_outgoing_to_abroad_inactive_days", "sms_incoming_count", "sms_incoming_spendings", "sms_incoming_from_abroad_count", "sms_incoming_from_abroad_spendings", "gprs_session_count", "gprs_usage", "gprs_spendings", "gprs_inactive_days", "last_100_reloads_count", "last_100_reloads_sum", "last_100_calls_outgoing_duration", "last_100_calls_outgoing_to_onnet_duration", "last_100_calls_outgoing_to_offnet_duration", "last_100_calls_outgoing_to_abroad_duration", "last_100_sms_outgoing_count", "last_100_sms_outgoing_to_onnet_count", "last_100_sms_outgoing_to_offnet_count", "last_100_sms_outgoing_to_abroad_count", "last_100_gprs_usage")(*map(float, sf))


# In[86]:

ScaledDF = (
    FeaturizedDF
    .select("features")
    .map(lambda r: r.features)
    .map(lambda Features: pyspark.sql.Row("user_lifetime", "user_no_outgoing_activity_in_days", "user_account_balance_last", "user_spendings", "reloads_inactive_days", "reloads_count", "reloads_sum", "calls_outgoing_count", "calls_outgoing_spendings", "calls_outgoing_duration", "calls_outgoing_spendings_max", "calls_outgoing_duration_max", "calls_outgoing_inactive_days", "calls_outgoing_to_onnet_count", "calls_outgoing_to_onnet_spendings", "calls_outgoing_to_onnet_duration", "calls_outgoing_to_onnet_inactive_days", "calls_outgoing_to_offnet_count", "calls_outgoing_to_offnet_spendings", "calls_outgoing_to_offnet_duration", "calls_outgoing_to_offnet_inactive_days", "calls_outgoing_to_abroad_count", "calls_outgoing_to_abroad_spendings", "calls_outgoing_to_abroad_duration", "calls_outgoing_to_abroad_inactive_days", "sms_outgoing_count", "sms_outgoing_spendings", "sms_outgoing_spendings_max", "sms_outgoing_inactive_days", "sms_outgoing_to_onnet_count", "sms_outgoing_to_onnet_spendings", "sms_outgoing_to_onnet_inactive_days", "sms_outgoing_to_offnet_count", "sms_outgoing_to_offnet_spendings", "sms_outgoing_to_offnet_inactive_days", "sms_outgoing_to_abroad_count", "sms_outgoing_to_abroad_spendings", "sms_outgoing_to_abroad_inactive_days", "sms_incoming_count", "sms_incoming_spendings", "sms_incoming_from_abroad_count", "sms_incoming_from_abroad_spendings", "gprs_session_count", "gprs_usage", "gprs_spendings", "gprs_inactive_days", "last_100_reloads_count", "last_100_reloads_sum", "last_100_calls_outgoing_duration", "last_100_calls_outgoing_to_onnet_duration", "last_100_calls_outgoing_to_offnet_duration", "last_100_calls_outgoing_to_abroad_duration", "last_100_sms_outgoing_count", "last_100_sms_outgoing_to_onnet_count", "last_100_sms_outgoing_to_offnet_count", "last_100_sms_outgoing_to_abroad_count", "last_100_gprs_usage")(*map(float, Features)))
    .toDF()
)
ScaledDF.show(5)


# In[87]:

from pyspark.mllib.clustering import KMeans
from pyspark.mllib.linalg import DenseVector


# In[88]:

aggUsageScaledDF.rdd.take(5)


# In[89]:

featuresRDD = aggUsageScaledDF.rdd.map(lambda row: DenseVector([row.user_lifetime,	row.user_no_outgoing_activity_in_days,	row.user_account_balance_last,	row.user_spendings,	row.reloads_inactive_days,	row.reloads_count,	row.reloads_sum,	row.calls_outgoing_count,	row.calls_outgoing_spendings,	row.calls_outgoing_duration,	row.calls_outgoing_spendings_max,	row.calls_outgoing_duration_max,	row.calls_outgoing_inactive_days,	row.calls_outgoing_to_onnet_count,	row.calls_outgoing_to_onnet_spendings,	row.calls_outgoing_to_onnet_duration,	row.calls_outgoing_to_onnet_inactive_days,	row.calls_outgoing_to_offnet_count,	row.calls_outgoing_to_offnet_spendings,	row.calls_outgoing_to_offnet_duration,	row.calls_outgoing_to_offnet_inactive_days,	row.calls_outgoing_to_abroad_count,	row.calls_outgoing_to_abroad_spendings,	row.calls_outgoing_to_abroad_duration,	row.calls_outgoing_to_abroad_inactive_days,	row.sms_outgoing_count,	row.sms_outgoing_spendings,	row.sms_outgoing_spendings_max,	row.sms_outgoing_inactive_days,	row.sms_outgoing_to_onnet_count,	row.sms_outgoing_to_onnet_spendings,	row.sms_outgoing_to_onnet_inactive_days,	row.sms_outgoing_to_offnet_count,	row.sms_outgoing_to_offnet_spendings,	row.sms_outgoing_to_offnet_inactive_days,	row.sms_outgoing_to_abroad_count,	row.sms_outgoing_to_abroad_spendings,	row.sms_outgoing_to_abroad_inactive_days,	row.sms_incoming_count,	row.sms_incoming_spendings,	row.sms_incoming_from_abroad_count,	row.sms_incoming_from_abroad_spendings,	row.gprs_session_count,	row.gprs_usage,	row.gprs_spendings,	row.gprs_inactive_days,	row.last_100_reloads_count,	row.last_100_reloads_sum,	row.last_100_calls_outgoing_duration,	row.last_100_calls_outgoing_to_onnet_duration,	row.last_100_calls_outgoing_to_offnet_duration,	row.last_100_calls_outgoing_to_abroad_duration,	row.last_100_sms_outgoing_count,	row.last_100_sms_outgoing_to_onnet_count,	row.last_100_sms_outgoing_to_offnet_count,	row.last_100_sms_outgoing_to_abroad_count, row.last_100_gprs_usage]))
featuresRDD.take(5)


# In[90]:

firstModel = KMeans.train(
    featuresRDD, k=2, maxIterations=20, 
    runs=10, initializationMode="k-means||")


# In[91]:

type(firstModel)


# In[92]:

firstModel.centers


# In[93]:

firstModel.computeCost(featuresRDD)


# In[94]:

from pyspark.sql import Row


# In[95]:

WithPredictionDF = (
    aggUsageScaledDF.rdd.map(lambda r: Row(*(tuple(r) + (firstModel.predict(DenseVector([r.user_lifetime,	r.user_no_outgoing_activity_in_days,	r.user_account_balance_last,	r.user_spendings,	r.reloads_inactive_days,	r.reloads_count,	r.reloads_sum,	r.calls_outgoing_count,	r.calls_outgoing_spendings,	r.calls_outgoing_duration,	r.calls_outgoing_spendings_max,	r.calls_outgoing_duration_max,	r.calls_outgoing_inactive_days,	r.calls_outgoing_to_onnet_count,	r.calls_outgoing_to_onnet_spendings,	r.calls_outgoing_to_onnet_duration,	r.calls_outgoing_to_onnet_inactive_days,	r.calls_outgoing_to_offnet_count,	r.calls_outgoing_to_offnet_spendings,	r.calls_outgoing_to_offnet_duration,	r.calls_outgoing_to_offnet_inactive_days,	r.calls_outgoing_to_abroad_count,	r.calls_outgoing_to_abroad_spendings,	r.calls_outgoing_to_abroad_duration,	r.calls_outgoing_to_abroad_inactive_days,	r.sms_outgoing_count,	r.sms_outgoing_spendings,	r.sms_outgoing_spendings_max,	r.sms_outgoing_inactive_days,	r.sms_outgoing_to_onnet_count,	r.sms_outgoing_to_onnet_spendings,	r.sms_outgoing_to_onnet_inactive_days,	r.sms_outgoing_to_offnet_count,	r.sms_outgoing_to_offnet_spendings,	r.sms_outgoing_to_offnet_inactive_days,	r.sms_outgoing_to_abroad_count,	r.sms_outgoing_to_abroad_spendings,	r.sms_outgoing_to_abroad_inactive_days,	r.sms_incoming_count,	r.sms_incoming_spendings,	r.sms_incoming_from_abroad_count,	r.sms_incoming_from_abroad_spendings,	r.gprs_session_count,	r.gprs_usage,	r.gprs_spendings,	r.gprs_inactive_days,	r.last_100_reloads_count,	r.last_100_reloads_sum,	r.last_100_calls_outgoing_duration,	r.last_100_calls_outgoing_to_onnet_duration,	r.last_100_calls_outgoing_to_offnet_duration,	r.last_100_calls_outgoing_to_abroad_duration,	r.last_100_sms_outgoing_count,	r.last_100_sms_outgoing_to_onnet_count,	r.last_100_sms_outgoing_to_offnet_count,	r.last_100_sms_outgoing_to_abroad_count, r.last_100_gprs_usage])),))))
).toDF(aggUsageScaledDF.columns + ["prediction"])


# In[96]:

def addPredictionColumn(row, model, featureColumns):
    rowDict = row.asDict()
    features = DenseVector([rowDict[c] for c in featureColumns])
    predictedValue = model.predict(features)
    allValues = tuple(row) + (predictedValue,)
    return Row(*allValues)


# In[97]:

WithPredictionDF = (
    aggUsageScaledDF.rdd.map(lambda row: addPredictionColumn(row, firstModel, ["user_lifetime", "user_no_outgoing_activity_in_days", "user_account_balance_last", "user_spendings", "reloads_inactive_days", "reloads_count", "reloads_sum", "calls_outgoing_count", "calls_outgoing_spendings", "calls_outgoing_duration", "calls_outgoing_spendings_max", "calls_outgoing_duration_max", "calls_outgoing_inactive_days", "calls_outgoing_to_onnet_count", "calls_outgoing_to_onnet_spendings", "calls_outgoing_to_onnet_duration", "calls_outgoing_to_onnet_inactive_days", "calls_outgoing_to_offnet_count", "calls_outgoing_to_offnet_spendings", "calls_outgoing_to_offnet_duration", "calls_outgoing_to_offnet_inactive_days", "calls_outgoing_to_abroad_count", "calls_outgoing_to_abroad_spendings", "calls_outgoing_to_abroad_duration", "calls_outgoing_to_abroad_inactive_days", "sms_outgoing_count", "sms_outgoing_spendings", "sms_outgoing_spendings_max", "sms_outgoing_inactive_days", "sms_outgoing_to_onnet_count", "sms_outgoing_to_onnet_spendings", "sms_outgoing_to_onnet_inactive_days", "sms_outgoing_to_offnet_count", "sms_outgoing_to_offnet_spendings", "sms_outgoing_to_offnet_inactive_days", "sms_outgoing_to_abroad_count", "sms_outgoing_to_abroad_spendings", "sms_outgoing_to_abroad_inactive_days", "sms_incoming_count", "sms_incoming_spendings", "sms_incoming_from_abroad_count", "sms_incoming_from_abroad_spendings", "gprs_session_count", "gprs_usage", "gprs_spendings", "gprs_inactive_days", "last_100_reloads_count", "last_100_reloads_sum", "last_100_calls_outgoing_duration", "last_100_calls_outgoing_to_onnet_duration", "last_100_calls_outgoing_to_offnet_duration", "last_100_calls_outgoing_to_abroad_duration", "last_100_sms_outgoing_count", "last_100_sms_outgoing_to_onnet_count", "last_100_sms_outgoing_to_offnet_count", "last_100_sms_outgoing_to_abroad_count", "last_100_gprs_usage"]))
).toDF(aggUsageScaledDF.columns + ["prediction"])


# In[98]:

WithPredictionDF.take(5)


# In[99]:

list(enumerate(firstModel.centers))


# In[100]:

abs(firstModel.centers[0] - firstModel.centers[1])


# In[101]:

kValues = [2,3,4,5,6,8,10,12]
models = [KMeans.train(featuresRDD, k=k) for k in kValues]
WSSSEs = [m.computeCost(featuresRDD) for m in models]

rowsWSSSSE = list(zip(kValues, map(float, WSSSEs)))
WSSSEDF = sc.parallelize(rowsWSSSSE).toDF(["K", "WSSSE"])


# In[102]:

print("K reikšmės")
print(kValues)
print("\nModelių objektai")
print(models)
print("\nS_k reikšmės")
print(WSSSEs)
print("\nRezultatų eilutės su K ir S_k reikšmėmis")
print(rowsWSSSSE)
print("\npyspark.sql.DataFrame turinys su rezultatais")
WSSSEDF.show()


# In[103]:

gg.ggplot(gg.aes(x="K", y="WSSSE"), data=WSSSEDF.toPandas()) + gg.geom_line()


# In[104]:

gg.ggplot(gg.aes(x="K", y="WSSSE"), data=WSSSEDF.where(WSSSEDF["K"] > 2).toPandas()) + gg.geom_line()


# In[105]:

import collections


# In[106]:

kmeansWSSSEResults = collections.namedtuple("KmeansWSSSEResults", ["ks", "WSSSEs", "models"])

def kmeansWSSSEsByK(featuresRDD, kValues):
    models = [KMeans.train(featuresRDD, k=k) for k in kValues]
    WSSSEs = [m.computeCost(featuresRDD) for m in models]
    return kmeansWSSSEResults(kValues, WSSSEs, models)


# In[107]:

ks, wssses, models = kmeansWSSSEsByK(featuresRDD, [2, 3, 4, 5, 6, 8, 10, 12])


# In[108]:

print(ks, "\n")
print(wssses, "\n")
print(models, "\n")


# In[109]:

results = kmeansWSSSEsByK(featuresRDD, [2, 3, 4, 5, 6, 8, 10, 12])


# In[110]:

print(results.ks, "\n")
print(results.WSSSEs, "\n")
print(results.models, "\n")


# In[111]:

bestModel = KMeans.train(
    featuresRDD, k=5, maxIterations=20, 
    runs=10, initializationMode="k-means||")


# In[112]:

NewPredictionDF = (
    aggUsageScaledDF.rdd.map(lambda row: addPredictionColumn(row, bestModel, ["user_lifetime", "user_no_outgoing_activity_in_days", "user_account_balance_last", "user_spendings", "reloads_inactive_days", "reloads_count", "reloads_sum", "calls_outgoing_count", "calls_outgoing_spendings", "calls_outgoing_duration", "calls_outgoing_spendings_max", "calls_outgoing_duration_max", "calls_outgoing_inactive_days", "calls_outgoing_to_onnet_count", "calls_outgoing_to_onnet_spendings", "calls_outgoing_to_onnet_duration", "calls_outgoing_to_onnet_inactive_days", "calls_outgoing_to_offnet_count", "calls_outgoing_to_offnet_spendings", "calls_outgoing_to_offnet_duration", "calls_outgoing_to_offnet_inactive_days", "calls_outgoing_to_abroad_count", "calls_outgoing_to_abroad_spendings", "calls_outgoing_to_abroad_duration", "calls_outgoing_to_abroad_inactive_days", "sms_outgoing_count", "sms_outgoing_spendings", "sms_outgoing_spendings_max", "sms_outgoing_inactive_days", "sms_outgoing_to_onnet_count", "sms_outgoing_to_onnet_spendings", "sms_outgoing_to_onnet_inactive_days", "sms_outgoing_to_offnet_count", "sms_outgoing_to_offnet_spendings", "sms_outgoing_to_offnet_inactive_days", "sms_outgoing_to_abroad_count", "sms_outgoing_to_abroad_spendings", "sms_outgoing_to_abroad_inactive_days", "sms_incoming_count", "sms_incoming_spendings", "sms_incoming_from_abroad_count", "sms_incoming_from_abroad_spendings", "gprs_session_count", "gprs_usage", "gprs_spendings", "gprs_inactive_days", "last_100_reloads_count", "last_100_reloads_sum", "last_100_calls_outgoing_duration", "last_100_calls_outgoing_to_onnet_duration", "last_100_calls_outgoing_to_offnet_duration", "last_100_calls_outgoing_to_abroad_duration", "last_100_sms_outgoing_count", "last_100_sms_outgoing_to_onnet_count", "last_100_sms_outgoing_to_offnet_count", "last_100_sms_outgoing_to_abroad_count", "last_100_gprs_usage"]))
).toDF(aggUsageScaledDF.columns + ["prediction"])


# In[113]:

(
    gg.ggplot(gg.aes(x="user_spendings", y="user_account_balance_last", color="prediction"), data=NewPredictionDF.toPandas()) + 
    gg.geom_point() + 
    gg.ggtitle("K = 5")
)


# In[114]:

(
    gg.ggplot(gg.aes(x="user_spendings", y="user_lifetime", color="prediction"), data=NewPredictionDF.toPandas()) + 
    gg.geom_point() + 
    gg.ggtitle("K = 5")
)


# # Failų customer_usage_005 ir customer_churn_005 apjungimas

# In[115]:

churnDF = (
    sqlContext.read.format("com.databricks.spark.csv")
    .options(inferSchema=True, header=True)
    .load("data/customer_churn_005.csv")
)


# In[116]:

churnDF


# In[117]:

output=aggregatedUsageDF2.join(churnDF,['user_account_id'],"outer")


# In[118]:

output.first()


# In[119]:

no_year=output.drop('year')


# In[120]:

no_year.drop('month')


# In[121]:

no_year.toPandas().to_csv('finaldata.csv')


# In[122]:

no_year.toPandas().head()


# # Sprendimų medžiai (angl. Decision trees)

# In[123]:

import os
import sys
import json
import datetime

import pandas as pd

from pyspark import ml
from pyspark import mllib

from pyspark.sql.types import StringType
from pyspark.ml.feature import StringIndexer
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.pipeline import Pipeline
from pyspark.ml.classification import RandomForestClassifier
from pyspark.ml.tuning import ParamGridBuilder
from pyspark.ml.tuning import CrossValidator
from pyspark.ml.evaluation import MulticlassClassificationEvaluator


# In[124]:

data = sqlContext.read.load("finaldata.csv", format="com.databricks.spark.csv", header=True, inferSchema=True).select(
["user_lifetime", "user_no_outgoing_activity_in_days", "user_account_balance_last", "user_spendings", "reloads_inactive_days", "reloads_count", "reloads_sum", "calls_outgoing_count",
"calls_outgoing_spendings", "calls_outgoing_duration", "calls_outgoing_spendings_max", "calls_outgoing_duration_max", "calls_outgoing_inactive_days", "calls_outgoing_to_onnet_count", "calls_outgoing_to_onnet_spendings", "calls_outgoing_to_onnet_duration", "calls_outgoing_to_onnet_inactive_days", "calls_outgoing_to_offnet_count", "calls_outgoing_to_offnet_spendings", "calls_outgoing_to_offnet_duration", "calls_outgoing_to_offnet_inactive_days", "calls_outgoing_to_abroad_count", "calls_outgoing_to_abroad_spendings", "calls_outgoing_to_abroad_duration", "calls_outgoing_to_abroad_inactive_days", "sms_outgoing_count", "sms_outgoing_spendings", "sms_outgoing_spendings_max", "sms_outgoing_inactive_days", "sms_outgoing_to_onnet_count", "sms_outgoing_to_onnet_spendings", "sms_outgoing_to_onnet_inactive_days", "sms_outgoing_to_offnet_count", "sms_outgoing_to_offnet_spendings", "sms_outgoing_to_offnet_inactive_days", "sms_outgoing_to_abroad_count", "sms_outgoing_to_abroad_spendings", "sms_outgoing_to_abroad_inactive_days", "sms_incoming_count", "sms_incoming_spendings", "sms_incoming_from_abroad_count", "sms_incoming_from_abroad_spendings", "gprs_session_count", "gprs_usage", "gprs_spendings", "gprs_inactive_days", "last_100_reloads_count", "last_100_reloads_sum", "last_100_calls_outgoing_duration", "last_100_calls_outgoing_to_onnet_duration", "last_100_calls_outgoing_to_offnet_duration", "last_100_calls_outgoing_to_abroad_duration", "last_100_sms_outgoing_count", "last_100_sms_outgoing_to_onnet_count", "last_100_sms_outgoing_to_offnet_count", "last_100_sms_outgoing_to_abroad_count", "last_100_gprs_usage", "user_intake", "user_has_outgoing_calls", "user_has_outgoing_sms", "user_use_gprs", "user_does_reload", "n_months", "churn"]
)
data.show(5)


# In[125]:

from pyspark.ml.feature import VectorAssembler


# In[126]:

assembler = VectorAssembler(inputCols=["user_lifetime", "user_no_outgoing_activity_in_days", "user_account_balance_last", "user_spendings", "reloads_inactive_days", "reloads_count", "reloads_sum", "calls_outgoing_count", "calls_outgoing_spendings", "calls_outgoing_duration", "calls_outgoing_spendings_max", "calls_outgoing_duration_max", "calls_outgoing_inactive_days", "calls_outgoing_to_onnet_count",
"calls_outgoing_to_onnet_spendings", "calls_outgoing_to_onnet_duration", "calls_outgoing_to_onnet_inactive_days", "calls_outgoing_to_offnet_count", "calls_outgoing_to_offnet_spendings", "calls_outgoing_to_offnet_duration", "calls_outgoing_to_offnet_inactive_days", "calls_outgoing_to_abroad_count", "calls_outgoing_to_abroad_spendings", "calls_outgoing_to_abroad_duration", "calls_outgoing_to_abroad_inactive_days", "sms_outgoing_count", "sms_outgoing_spendings", "sms_outgoing_spendings_max", "sms_outgoing_inactive_days", "sms_outgoing_to_onnet_count", "sms_outgoing_to_onnet_spendings", "sms_outgoing_to_onnet_inactive_days", "sms_outgoing_to_offnet_count", "sms_outgoing_to_offnet_spendings", "sms_outgoing_to_offnet_inactive_days", "sms_outgoing_to_abroad_count", "sms_outgoing_to_abroad_spendings", "sms_outgoing_to_abroad_inactive_days", "sms_incoming_count", "sms_incoming_spendings", "sms_incoming_from_abroad_count", "sms_incoming_from_abroad_spendings", "gprs_session_count", "gprs_usage", "gprs_spendings", "gprs_inactive_days", "last_100_reloads_count", "last_100_reloads_sum", "last_100_calls_outgoing_duration", "last_100_calls_outgoing_to_onnet_duration", "last_100_calls_outgoing_to_offnet_duration", "last_100_calls_outgoing_to_abroad_duration", "last_100_sms_outgoing_count", "last_100_sms_outgoing_to_onnet_count", "last_100_sms_outgoing_to_offnet_count", "last_100_sms_outgoing_to_abroad_count", "last_100_gprs_usage", "user_intake", "user_has_outgoing_calls", "user_has_outgoing_sms", "user_use_gprs", "user_does_reload"], outputCol="features")


# In[127]:

dataFeaturizedDF = assembler.transform(data)
dataFeaturizedDF.show(5)


# In[128]:

dataFeaturizedDF.first()


# In[129]:

from pyspark.mllib.linalg import DenseVector


# In[130]:

data.rdd.take(5)


# In[131]:

featuresRDD = data.rdd.map(lambda row: DenseVector([row.user_lifetime, row.user_no_outgoing_activity_in_days, row.user_account_balance_last,
row.user_spendings, row.reloads_inactive_days, row.reloads_count, row.reloads_sum, row.calls_outgoing_count, row.calls_outgoing_spendings, row.calls_outgoing_duration, row.calls_outgoing_spendings_max, row.calls_outgoing_duration_max, row.calls_outgoing_inactive_days, row.calls_outgoing_to_onnet_count, row.calls_outgoing_to_onnet_spendings, row.calls_outgoing_to_onnet_duration, row.calls_outgoing_to_onnet_inactive_days, row.calls_outgoing_to_offnet_count, row.calls_outgoing_to_offnet_spendings, row.calls_outgoing_to_offnet_duration, row.calls_outgoing_to_offnet_inactive_days, row.calls_outgoing_to_abroad_count, row.calls_outgoing_to_abroad_spendings, row.calls_outgoing_to_abroad_duration, row.calls_outgoing_to_abroad_inactive_days, row.sms_outgoing_count, row.sms_outgoing_spendings, row.sms_outgoing_spendings_max, row.sms_outgoing_inactive_days, row.sms_outgoing_to_onnet_count, row.sms_outgoing_to_onnet_spendings, row.sms_outgoing_to_onnet_inactive_days, row.sms_outgoing_to_offnet_count, row.sms_outgoing_to_offnet_spendings, row.sms_outgoing_to_offnet_inactive_days, row.sms_outgoing_to_abroad_count, row.sms_outgoing_to_abroad_spendings, row.sms_outgoing_to_abroad_inactive_days, row.sms_incoming_count, row.sms_incoming_spendings, row.sms_incoming_from_abroad_count, row.sms_incoming_from_abroad_spendings, row.gprs_session_count, row.gprs_usage, row.gprs_spendings, row.gprs_inactive_days, row.last_100_reloads_count, row.last_100_reloads_sum, row.last_100_calls_outgoing_duration, row.last_100_calls_outgoing_to_onnet_duration, row.last_100_calls_outgoing_to_offnet_duration, row.last_100_calls_outgoing_to_abroad_duration, row.last_100_sms_outgoing_count, row.last_100_sms_outgoing_to_onnet_count, row.last_100_sms_outgoing_to_offnet_count, row.last_100_sms_outgoing_to_abroad_count, row.last_100_gprs_usage, row.user_intake, row.user_has_outgoing_calls, row.user_has_outgoing_sms, row.user_use_gprs, row.user_does_reload]))
featuresRDD.take(5)


# In[132]:

churn = data.map(lambda row: row[-1])
churn.take(5)


# In[133]:

data.toPandas().head()


# In[134]:

transformedData = churn.zip(featuresRDD)
transformedData.take(5)


# In[135]:

from pyspark.mllib.regression import LabeledPoint


# In[136]:

temp = data.map(lambda line:LabeledPoint(line[-1],[line[:63]]))
temp.take(5)


# In[137]:

from pyspark.mllib.tree import DecisionTree, DecisionTreeModel
from pyspark.mllib.util import MLUtils


# In[138]:

model = DecisionTree.trainClassifier(temp, numClasses=2, categoricalFeaturesInfo={},impurity='gini', maxDepth=5, maxBins=32)


# In[139]:

(trainingData, testData) = temp.randomSplit([0.7, 0.3])


# In[140]:

model = DecisionTree.trainClassifier(trainingData, numClasses=2, categoricalFeaturesInfo={},impurity='gini', maxDepth=5, maxBins=32)


# In[141]:

predictions = model.predict(testData.map(lambda x: x.features))


# In[142]:

labelsAndPredictions = testData.map(lambda lp: lp.label).zip(predictions)


# In[143]:

testErr = labelsAndPredictions.filter(lambda seq: seq[0] != seq[1]).count() / float(testData.count())


# In[144]:

print('Test Error = ' + str(testErr))


# In[145]:

print('Learned classification tree model:')


# In[146]:

print(model.toDebugString())

